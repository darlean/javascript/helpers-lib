import * as app from '@darlean/app-lib';

export class TimeService implements app.ITimeService {
  protected time: app.ITime;

  constructor(time: app.ITime) {
    this.time = time;
  }

  public async nodeTime(): Promise<number> {
    return this.time.machineTime();
  }

  public async nodeTicks(): Promise<number> {
    return this.time.machineTicks();
  }

  public async clusterTime(): Promise<number> {
    throw new Error('Not implemented');
  }

  public async clusterTicks(): Promise<number> {
    throw new Error('Not implemented');
  }

  public async sleep(ms: number): Promise<void> {
    await this.time.sleep(ms);
  }

  public async noop(): Promise<void> {
    await this.time.noop();
  }
}
