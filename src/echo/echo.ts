import * as applib from '@darlean/app-lib';

export function obtainFactories(logger: applib.ILogger): applib.ActorFactory[] {
  return [
    applib.actorFactory(logger, {
      capacity: 1000000,
      clazz: EchoActor,
    }),
  ];
}

export interface IEchoRequest {
  value: unknown;
}

export interface IEchoResponse {
  value: unknown;
}

@applib.actor({ name: 'io.darlean.EchoActor', appBindIndex: 0 })
export class EchoActor extends applib.BaseActor {
  @applib.action({ name: 'echo', locking: 'shared' })
  public async echo(data: IEchoRequest): Promise<IEchoResponse> {
    return {
      value: data.value,
    };
  }
}
