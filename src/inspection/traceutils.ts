export interface IKnot {
  moment: number;
  value: number;
}

export type Knots = IKnot[];

/**
 * Merges an existing knots array with a new range.
 * @param a Array of knots with value=1 indicates start of range and value=-1 indicates end of range
 * @param b Array of knots with value=1 indicates start of range and value=-1 indicates end of range
 * @returns Array of knots with value=1 for start of range and value=-1 for end of range
 */
export function mergeKnots(a: Knots, b: Knots): Knots {
  const temp = [...a, ...b];
  orderKnots(temp);
  return flattenKnots(temp);
}

/**
 * Derives an array of knots where the value `2` denotes part of range without underlying
 * data, value `1` denotes part of range with underlying data, value `0` denotes not
 * part of range (and also no underlying data), and value `-1` denotes not part of range,
 * but underlying data.
 * @param rangeFrom
 * @param rangeTo
 * @param underlying
 * @returns
 */
export function overlapKnots(rangeFrom: number, rangeTo: number, underlying: Knots): Knots {
  const temp = [
    ...underlying.map((v) => ({ moment: v.moment, value: -v.value })),
    { moment: rangeFrom, value: 2 },
    { moment: rangeTo, value: -2 },
  ];
  orderKnots(temp);
  return cumulateKnots(temp);
}

export function orderKnots(knots: Knots): void {
  knots.sort((a, b) => a.moment - b.moment || b.value - a.value);
}

export function flattenKnots(orderedKnots: Knots): Knots {
  const result: IKnot[] = [];
  let cum = 0;
  for (const knot of orderedKnots) {
    const newCum = cum + knot.value;
    if (cum === 0 && newCum > 0) {
      result.push({ moment: knot.moment, value: 1 });
    } else if (cum > 0 && newCum <= 0) {
      result.push({ moment: knot.moment, value: -1 });
    }
    cum = newCum;
  }
  return result;
}

export function cumulateKnots(orderedKnots: Knots): Knots {
  const result: IKnot[] = [];
  let cum = 0;
  for (const knot of orderedKnots) {
    cum += knot.value;
    result.push({ moment: knot.moment, value: cum });
  }
  // console.log('CUMULATE', orderedKnots, result);
  return result;
}
