import * as app from '@darlean/app-lib';
import { IAppInfo } from '@darlean/app-lib';
import { IGetTraceRequest, IGetTraceResponse } from '@darlean/app-lib/lib/interfaces/tracing';
import * as express from 'express';
import { Server } from 'http';
import { IGetLogEventsResponse, IGetStatsResponse, ILogFilter } from '../logging/logging';
import { Knots, mergeKnots, overlapKnots } from './traceutils';

const css = [
  '<meta name="viewport" content="width=device-width, initial-scale=1">',
  '<link rel="icon" href="https://gitlab.com/uploads/-/system/project/avatar/30180687/logo.png?width=64">',
  '<style>',
  'body { font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Noto Sans", Ubuntu, Cantarell, "Helvetica Neue", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";}',
  'table { border-collapse: collapse; }',
  'td,th { padding-left: 0.5rem; padding-right: 0.5rem; text-align: start; vertical-align: top; }',
  'tr:nth-child(even) {background: #EEE}',
  'tr:nth-child(odd) {background: #FFF}',
  'thead th { position: sticky; top: 0px; background-color: rgba(200,200,200,0.9); }',
  'table table tr:nth-child(even) {background: none}',
  'table table tr:nth-child(odd) {background: none}',
  'table table {font-size: smaller}',
  '.value { opacity: 0.7; } ',
  '.navbar { font-size: smaller }',
  'a:link {}',
  'a:visited {}',
  'a:hover { background-color: #eee; }',
  'a:active { background-color: #eee; }',
  'a { color: #605226; margin-left: 0.7em; margin-right: 0.7em; border-radius: 5px; padding-left: 5px; padding-right: 5px; padding-top: 3px; padding-bottom: 3px; text-decoration: none}',
  'div.remark {font-size: smaller; font-style: italic; margin-left: 2rem; margin-top: 1rem; margin-bottom: 1rem;}',
  'div.remark.header {font-size: smaller; font-style: italic; margin-left: 2rem; margin-top: 1rem; margin-bottom: 1rem;}',
  'div.remark.body {opacity: 0.7;}',
  'div.remark input[type="button"] { font-variant: all-small-caps; margin-left: 1rem; margin-right: 1rem; }',
  'div.remark input[type="submit"] { font-variant: all-small-caps; margin-left: 1rem; margin-right: 1rem; }',
  'div.remark input[type="reset"] { font-variant: all-small-caps; margin-left: 1rem; margin-right: 1rem; }',
  'div.bar { background-color: #008800; height: 10px; }',
  'div.bar-1 { opacity: 0.3 }',
  'div.bar--1 { opacity: 0.1 }',
  'table.tiny td,th {font-size: 11pt; }',
  'td.number { text-align: right }',
];
for (let i = 0; i < 100; i++) {
  css.push(`.indent-${i} { padding-left: ${(i * 0.5).toFixed(1)}rem; }`);
}
css.push('</style>');

const navbar = [
  '<div class="navbar">',
  '<img style="vertical-align:middle; border-radius: 7px;" src="https://gitlab.com/uploads/-/system/project/avatar/30180687/logo.png?width=32" width="32" height="32">',
  '<span style="style="vertical-align:middle">',
  '<span style="margin-left: 0.5rem; margin-right: 2rem;"><b>DARLEAN</b></span>',
  '<a href="/actorinfo">Actor Info</a>',
  // ' | ',
  '<a href="/placement">Actor Placement</a>',
  '<a href="/state/actor">State</a>',
  '<a href="/logging/events/flat">Logging</a>',
  '</span></div>',
];

export interface IInspectionOptions {
  port: number;
  lockNodes: string[];
}

function escape(value?: string): string {
  if (!value) {
    return '';
  }
  return app.replaceAll(value, '<', '&lt;');
}

/*
function attributesToTable2(attributes: app.IAttributes): string[] {
  const result = [];
  result.push('<table>');
  for (const [key, value] of Object.entries(attributes)) {
    result.push('<tr>');
    result.push('<td>' + escape(key) + '</th>');
    result.push('<td>' + escape(value as string) + '</th>');
    result.push('</tr>');  
  }
  result.push('</table>');
  return result;
}*/

function attributesToTable(attributes: app.IAttributes): string[] {
  const result = [];
  /*result.push('<tr>');
  result.push('<th>Key</th>');
  result.push('<th>Value</th>');
  result.push('</tr>');*/
  for (const [key, value] of Object.entries(attributes)) {
    result.push('<div>');
    result.push('<span class="key">' + escape(key ?? '') + ': ' + '</span>');
    result.push('<span class="value">' + escape((value as string) ?? '') + '</span>');
    result.push('</div>');
  }
  return result;
}

export function obtainFactories(logger: app.ILogger, options: IInspectionOptions): app.ActorFactory[] {
  let server: Server | undefined;
  return [
    new app.ActorFactory(
      logger,
      'InspectionService',
      'service',
      1000,
      [],
      (opts) => new InspectionService(opts),
      async (runtime) => {
        logger.info('STARTING EXPRESS');
        const appl = express();
        appl.use(express.urlencoded({ extended: true }));
        appl.get('/', (req, resp) => {
          const html = [];
          html.push('<html><head>');
          html.push('<title>DARLEAN</title>');
          html.push(css.join('\n'));
          html.push('</head><body>');
          html.push(navbar.join('\n'));
          html.push('</body></html>');
          resp.send(html.join('\n'));
          resp.end();
        });
        appl.get('/actorinfo', async (req, resp) => {
          try {
            const results = await runtime.dispatchAction<void, IAppInfo[]>(logger, {
              actorType: 'RuntimeService',
              actorId: [],
              actionName: 'InspectActorInfo',
            });
            const html = [];
            html.push('<html><head>');
            html.push('<title>DARLEAN</title>');
            html.push(css.join('\n'));
            html.push('</head><body>');
            html.push(navbar.join('\n'));
            html.push('<h1>Actor information</h1>');
            for (const appInfo of results.sort((a, b) => a.id.localeCompare(b.id))) {
              html.push('<h2>' + appInfo.id + '</h2>');
              if (appInfo.supportedActors?.length > 0) {
                html.push('<table>');
                html.push('<tr>');
                html.push('<th>Name</th>');
                html.push('<th>Multiplicity</th>');
                html.push('<th>Placement</th>');
                html.push('<th>Status</th>');
                html.push('<th>Availability</th>');
                html.push('<th>AppBindIndex</th>');
                html.push('</tr>');

                for (const actor of appInfo.supportedActors.sort((a, b) => a.name.localeCompare(b.name))) {
                  html.push('<tr>');
                  html.push('<td>' + escape(actor.name) + '</td>');
                  html.push('<td>' + escape(actor.multiplicity) + '</td>');
                  html.push('<td>' + escape(actor.placement) + '</td>');
                  html.push('<td>' + escape(actor.status) + '</td>');
                  html.push('<td>' + escape(actor.availability.toString()) + '</td>');
                  html.push('<td>' + escape(actor.appBindIndex?.toString()) + '</td>');
                  html.push('</tr>');
                }
                html.push('</table>');
              } else {
                html.push('<div class="remark">There are no actors defined by ' + appInfo.id + '.</div>');
              }
            }
            html.push('</body></html>');
            resp.send(html.join('\n'));
            resp.end();
          } catch (e) {
            resp.status(500).end();
          }
        });
        appl.get('/placement', async (req, resp) => {
          try {
            const results = await runtime.dispatchAction<void, app.interface_locking.IAggregatedSingleLock[]>(logger, {
              actorType: 'RuntimeService',
              actorId: [],
              actionName: 'InspectLocks',
            });
            const html = [];
            html.push('<html><head>');
            html.push('<title>DARLEAN</title>');
            html.push(css.join('\n'));
            html.push('</head><body>');
            html.push(navbar.join('\n'));
            html.push('<h1>Dynamic actor placement</h1>');
            html.push('<table>');
            html.push('<thead>');
            html.push('<tr>');
            html.push('<th>Actor Type</th>');
            html.push('<th>Actor Id</th>');
            for (const node of options.lockNodes) {
              html.push('<th>' + escape(node) + '</th>');
            }
            html.push('</tr>');
            html.push('</thead>');
            const locks = results;
            locks.sort((a, b) => JSON.stringify(a.id).localeCompare(JSON.stringify(b.id)));
            for (const lock of locks) {
              html.push('<tr>');
              html.push('<td>' + escape(lock.id[0]) + '</td>');
              html.push('<td>' + escape(JSON.stringify(lock.id.slice(1))) + '</td>');
              for (const node of options.lockNodes) {
                const holder = lock.holders.find((v) => v.source === node);
                if (holder) {
                  html.push('<td>' + escape(holder.holder + ' (' + (holder.ttl * 0.001).toFixed(0) + 's)') + '</td>');
                } else {
                  html.push('<td>&mdash;</td>');
                }
              }
              html.push('</tr>');
            }
            html.push('</table>');
            html.push('</body></html>');
            resp.send(html.join('\n'));
            resp.end();
          } catch (e) {
            resp.status(500).end();
          }
        });
        appl.get('/state/:compartment', async (req, resp) => {
          const compartment = req.params.compartment;
          try {
            logger.info('Getting state info for [Compartment]', () => ({ Compartment: compartment }));
            const results = await runtime.dispatchAction<
              app.interface_persistence.IInspectRequest,
              app.interface_persistence.IInspectResponse
            >(logger, {
              actorType: 'PersistenceService',
              actorId: [],
              actionName: 'inspect',
              data: {
                compartment,
                includeValues: false,
                shardLimit: 10000,
              },
            });
            const html = [];
            html.push('<html><head>');
            html.push('<title>DARLEAN</title>');
            html.push(css.join('\n'));
            html.push('</head><body>');
            html.push(navbar.join('\n'));
            html.push('<h1>State for ' + escape(compartment) + '</h1>');
            html.push('<table>');
            html.push('<thead>');
            html.push('<tr>');
            html.push('<th>Lookup Key</th>');
            html.push('<th>Sort Key</th>');
            html.push('<th>Shard</th>');
            html.push('</tr>');
            html.push('</thead>');
            for (const item of results.items) {
              html.push('<tr>');
              html.push('<td>' + escape(JSON.stringify(item.lookupKeys)) + '</td>');
              html.push('<td>' + escape(JSON.stringify(item.sortKeys)) + '</td>');
              html.push('<td>' + escape(item.shard.toFixed(0)) + '</td>');
              html.push('</tr>');
            }
            html.push('</table>');
            html.push('</body></html>');
            resp.send(html.join('\n'));
            resp.end();
          } catch (e) {
            logger.error('Error during getting state for [Compartment]: [Error]', () => ({
              Compartment: compartment,
              Error: e,
            }));
            resp.status(500).end();
          }
        });
        appl.get('/logging/events/:by', async (req, resp) => {
          try {
            logger.info('Request for log events');
            const results = await runtime.dispatchAction<void, IGetLogEventsResponse>(logger, {
              actorType: 'LoggingActor',
              actorId: [],
              actionName: 'GetLogEvents',
            });

            const tags: string[] = [];
            let by = req.params.by;
            if (by === 'flat') {
              by = '';
            }
            const bys = new Map();
            for (const event of results.events) {
              if (by) {
                const value = event.tags?.[by];
                if (value) {
                  bys.set(value, true);
                }
              }
              if (event.tags) {
                for (const tag of Object.keys(event.tags)) {
                  if (!tags.includes(tag)) {
                    tags.push(tag);
                  }
                }
              }
            }

            const byslist = Array.from(bys.keys()).sort();
            tags.sort();

            const html = [];
            html.push('<html><head>');
            html.push('<title>DARLEAN</title>');
            html.push(css.join('\n'));
            html.push('</head><body>');
            html.push(navbar.join('\n'));
            html.push('<h1>Logging</h1>');
            if (tags.length > 0) {
              html.push('<div class="remark">');
              html.push('<span class="header">Swimlanes:</span>');
              html.push('<span class="body">');
              html.push('<a href="flat">None</a>');
              for (const tag of tags) {
                html.push('<a href="' + tag + '">' + escape(tag) + '</a>');
              }
              html.push('</span>');
              html.push('</div>');
              html.push('<div class="remark">');
              html.push('<span class="header">Filter:</span>');
              html.push('<span class="body">');
              html.push('<a href="../selection">Event selection</a>');
              html.push('</span>');
              html.push('</div>');
            }
            html.push('<table>');
            html.push('<thead>');
            html.push('<tr>');
            html.push('<th>Time</th>');
            html.push('<th>Level</th>');
            for (const b of byslist) {
              html.push('<th>' + b + '</th>');
            }
            html.push('<th>Message</th>');
            html.push('<th>Arguments</th>');
            html.push('<th>Tags</th>');
            html.push('</tr>');
            html.push('</thead>');
            for (const event of results.events) {
              html.push('<tr>');
              html.push('<td>' + escape(event.moment) + '</td>');
              html.push('<td>' + escape(event.level) + '</td>');
              const b = by ? event.tags?.[by] : '';
              let found = false;
              for (const b2 of byslist) {
                if (b === b2) {
                  found = true;
                  html.push('<td>' + escape(event.message) + '</td>');
                } else {
                  html.push('<td></td>');
                }
              }
              if (!found) {
                html.push('<td>' + escape(event.message) + '</td>');
              } else {
                html.push('<td></td>');
              }
              html.push('<td>');
              if (event.args) {
                html.push(attributesToTable(event.args).join('\n'));
              }
              html.push('</td>');
              html.push('<td>');
              if (event.tags) {
                html.push(attributesToTable(event.tags).join('\n'));
              }
              html.push('</td>');
              html.push('</tr>');
            }
            html.push('</table>');
            html.push('</body></html>');
            resp.send(html.join('\n'));
            resp.end();
          } catch (e) {
            console.log('ERROR', e);
            logger.error('Error during getting log events: [Error]', () => ({
              Error: e,
            }));
            resp.status(500).end();
          }
        });
        appl.get('/logging/selection', async (req, resp) => {
          try {
            logger.info('Request for log selection');
            const results = await runtime.dispatchAction<void, IGetStatsResponse>(logger, {
              actorType: 'LoggingActor',
              actorId: [],
              actionName: 'GetStats',
            });

            const filter = await runtime.dispatchAction<void, ILogFilter>(logger, {
              actorType: 'LoggingActor',
              actorId: [],
              actionName: 'GetFilter',
            });

            const sorted = results.stats.sort((a, b) => a.message.localeCompare(b.message));

            const html = [];
            html.push('<html><head>');
            html.push('<title>DARLEAN</title>');
            html.push(css.join('\n'));
            html.push('</head><body>');
            html.push(navbar.join('\n'));
            html.push('<h1>Event selection and statistics</h1>');
            if (sorted.length === 0) {
              html.push('<div class="remark">Please wait a minute until the first statistics arrive</div>');
            } else {
              html.push('<form method="post">');
              html.push('<div class="remark">');
              html.push(
                '<input type="button" value="Clear All" onclick="document.querySelectorAll(\'select\').forEach((v) => v.value = \'\')"></input>',
              );
              html.push(
                '<input type="button" value="Show All" onclick="document.querySelectorAll(\'select\').forEach((v) => v.value = \'show\')"></input>',
              );
              html.push(
                '<input type="button" value="Hide All" onclick="document.querySelectorAll(\'select\').forEach((v) => v.value = \'hide\')"></input>',
              );
              html.push('</div>');
              html.push('<table>');
              html.push('<thead>');
              html.push('<tr>');
              html.push('<th>&nbsp;</th>');
              html.push('<th>Message</th>');
              html.push('<th>Count</th>');
              html.push('</tr>');
              html.push('</thead>');
              for (const item of sorted) {
                const value = filter.messages.find((v) => v.msg === item.message)?.action || '';
                html.push('<tr>');
                html.push(`<td><select name="${encodeURIComponent(item.message)}">`);
                html.push(`<option value="" ${value === '' ? 'selected' : ''}></option>`);
                html.push(`<option value="show" ${value === 'show' ? 'selected' : ''}>Show</option>`);
                html.push(`<option value="hide" ${value === 'hide' ? 'selected' : ''}>Hide</option>`);
                html.push('</select></td>');
                html.push('<td>' + escape(item.message) + '</td>');
                html.push('<td>' + escape(item.count.toFixed(0)) + '</td>');
                html.push('</tr>');
              }
              html.push('</table>');
              html.push('<div class="remark">');
              html.push('<input type="submit" value="Apply"></input>');
              html.push('<input type="reset" value="Reset"></input>');
              html.push('</div>');
              html.push('</form>');
            }
            html.push('</body></html>');
            resp.send(html.join('\n'));
            resp.end();
          } catch (e) {
            logger.error('Error during getting log stats: [Error]', () => ({
              Error: e,
            }));
            resp.status(500).end();
          }
        });

        appl.post('/logging/selection', async (req, resp) => {
          try {
            logger.info('Request for post log selection');
            const filter: ILogFilter = {
              messages: [],
            };
            console.log('BODY', req.body);
            for (const [option, value] of Object.entries(req.body)) {
              if (value === 'show') {
                filter.messages.push({
                  msg: decodeURIComponent(option),
                  action: 'show',
                });
              } else if (value === 'hide') {
                filter.messages.push({
                  msg: decodeURIComponent(option),
                  action: 'hide',
                });
              }
            }

            await runtime.dispatchAction<ILogFilter, void>(logger, {
              actorType: 'LoggingActor',
              actorId: [],
              actionName: 'SetFilter',
              data: filter,
            });

            const html = [];
            html.push('<html><head>');
            html.push('<title>DARLEAN</title>');
            html.push(css.join('\n'));
            html.push('</head><body>');
            html.push(navbar.join('\n'));
            html.push('<h1>Logging selection</h1>');
            html.push('<div class="remark">');
            html.push(
              'The new filter will become active within the next minute. Click <a href="events/flat">here</a> to go back to the logging.',
            );
            html.push('</div>');
            html.push('</body></html>');
            resp.send(html.join('\n'));
            resp.end();
          } catch (e) {
            logger.error('Error during getting log stats: [Error]', () => ({
              Error: e,
            }));
            resp.status(500).end();
          }
        });

        appl.get('/tracing/:cid/:root', async (req, resp) => {
          try {
            logger.info('Request for tracing');
            const results = await runtime.dispatchAction<IGetTraceRequest, IGetTraceResponse>(logger, {
              actorType: 'TracingActor',
              actorId: [],
              actionName: 'getTrace',
              data: { correlationId: req.params.cid },
            });

            const root = req.params.root;

            let roots: string[] = [];
            const nodes = new Map<string, { segment: app.interface_tracing.ISegment; children: string[] }>();
            for (const segment of results.segments) {
              nodes.set(segment.id, { segment, children: [] });
            }
            for (const segment of results.segments) {
              const parent = segment.parentId ? nodes.get(segment.parentId) : undefined;
              if (parent) {
                parent.children.push(segment.id);
              } else {
                roots.push(segment.id);
              }
            }

            if (root !== 'all') {
              roots = [root];
            }

            let minMoment: number | undefined;
            let maxMoment: number | undefined;

            const info = new Map<
              string,
              { knots: Knots; overlapKnots?: Knots; selfTime?: number; totalTime?: number; level: number }
            >();
            let maxSelfTime = 0;

            // eslint-disable-next-line no-inner-declarations
            function scanNode(segment: app.interface_tracing.ISegment, children: string[], indent: number): Knots {
              let knots: Knots = [];
              for (const child of children) {
                const c = nodes.get(child);
                if (c) {
                  const k = scanNode(c.segment, c.children, indent + 1);
                  if (k.length > 0) {
                    knots = mergeKnots(knots, k);
                  }
                }
              }

              let selfTime = 0;
              if (segment.startMoment !== undefined && segment.endMoment !== undefined) {
                const k2 = overlapKnots(segment.startMoment, segment.endMoment, knots);

                if (segment.startMoment && (minMoment === undefined || segment.startMoment < minMoment)) {
                  minMoment = segment.startMoment;
                }
                if (segment.endMoment && (maxMoment === undefined || segment.endMoment > maxMoment)) {
                  maxMoment = segment.endMoment;
                }

                for (let i = 0; i < k2.length - 1; i++) {
                  const k0 = k2[i];
                  if (k0.value !== 0) {
                    const k1 = k2[i + 1];
                    const x0 = k0.moment;
                    const x1 = k1.moment;
                    const w = x1 - x0;
                    if (k0.value === 2) {
                      selfTime += w;
                    }
                  }
                }
                if (selfTime > maxSelfTime) {
                  maxSelfTime = selfTime;
                }
                info.set(segment.id, {
                  knots,
                  level: indent,
                  overlapKnots: k2,
                  selfTime,
                  totalTime: segment.endMoment - segment.startMoment,
                });
                return mergeKnots(knots, [
                  { moment: segment.startMoment, value: 1 },
                  { moment: segment.endMoment, value: -1 },
                ]);
              }
              info.set(segment.id, {
                knots,
                level: indent,
              });
              return knots;
            }

            for (const root of roots) {
              const node = nodes.get(root);
              if (node) {
                scanNode(node.segment, node.children, 0);
              }
            }

            const span = minMoment !== undefined && maxMoment !== undefined ? maxMoment - minMoment : undefined;

            const html = [];
            // eslint-disable-next-line no-inner-declarations
            function processNode(segment: app.interface_tracing.ISegment, children: string[], indent: number): void {
              const inf = info.get(segment.id);
              if (!inf) {
                return;
              }
              html.push('<tr>');
              html.push(`<td>${inf.level}</td>`);
              indent++;

              if (
                span !== undefined &&
                segment.startMoment !== undefined &&
                segment.endMoment !== undefined &&
                minMoment !== undefined &&
                maxMoment !== undefined
              ) {
                html.push(`<td style="min-width: 250px; position: relative;">`);

                const k2 = overlapKnots(segment.startMoment, segment.endMoment, inf.knots);

                for (let i = 0; i < k2.length - 1; i++) {
                  const k0 = k2[i];
                  if (k0.value !== 0) {
                    const k1 = k2[i + 1];
                    const x0 = Math.round(((k0.moment - minMoment) * 200) / span);
                    const x1 = Math.round(((k1.moment - minMoment) * 200) / span);
                    const w = x1 - x0;
                    const safename = app.replaceAll(segment.name, '"', '&quot;');
                    const title = `From: ${(k0.moment - minMoment).toFixed(2)}ms, To: ${(k1.moment - minMoment).toFixed(
                      2,
                    )}ms, Duration: ${(k1.moment - k0.moment).toFixed(2)}ms, Level: ${indent}, Name: ${safename}`;
                    const flameColor = getFlameColor(inf.selfTime || 0);
                    html.push(
                      `<div class="bar bar-${k0.value}" style="position: absolute; top: 5px; left: ${x0}px; width: ${
                        w || 1
                      }px; background-color: ${flameColor};" title="${title}"></div>`,
                    );
                  }
                }
                html.push('</td>');
              } else {
                html.push('<td>-</td>');
              }

              html.push(`<td class="number">${inf.selfTime !== undefined ? inf.selfTime.toFixed(2) + 'ms' : '-'}</td>`);
              html.push(
                `<td class="number">${inf.totalTime !== undefined ? inf.totalTime.toFixed(2) + 'ms' : '-'}</td>`,
              );
              html.push(
                `<td class="indent-${inf.level}"><a href="${segment.id}">${app.replaceAll(
                  segment.name,
                  '<',
                  '&lt;',
                )}</a></td>`,
              );
              html.push(
                `<td>${
                  segment.attributes
                    ? Object.entries(segment.attributes)
                        .map((v) => v[0] + ': ' + JSON.stringify(v[1]))
                        .join(', ')
                    : ''
                }</td>`,
              );
              html.push('</tr>');

              for (const child of children) {
                const c = nodes.get(child);
                if (c) {
                  processNode(c.segment, c.children, indent + 1);
                }
              }
            }

            // eslint-disable-next-line no-inner-declarations
            function getFlameColor(selfTime: number): string {
              if (maxSelfTime === 0) {
                return '#999999';
              }
              const f = selfTime / maxSelfTime;
              const r = 255; //Math.round(f * 127 + 128);
              const g = Math.round(f * 255);
              const b = 0;
              return (
                '#' +
                r.toString(16).padStart(2, '0') +
                g.toString(16).padStart(2, '0') +
                b.toString(16).padStart(2, '0')
              );
            }

            html.push('<html><head>');
            html.push('<title>DARLEAN</title>');
            html.push(css.join('\n'));
            html.push('</head><body>');
            html.push(navbar.join('\n'));
            html.push('<h1>Tracing</h1>');
            html.push('<table class="tiny">');
            html.push('<thead>');
            html.push('<tr>');
            html.push('<th title="Level of the stack">Level</th>');
            html.push('<th>Timing</th>');
            html.push('<th title="Self Time (Time not spent in underlying segments)">Self</th>');
            html.push('<th title="Total Time (Total time spent in this segment)">Total</th>');
            html.push('<th>Item</th>');
            html.push('<th>Attributes</th>');
            html.push('</tr>');
            html.push('</thead>');
            for (const root of roots) {
              const node = nodes.get(root);
              if (node) {
                processNode(node.segment, node.children, 0);
              }
            }
            html.push('</table>');
            html.push('</body></html>');
            resp.send(html.join('\n'));
            resp.end();
          } catch (e) {
            console.log('ERROR', e);
            logger.error('Error during getting trace info: [Error]', () => ({
              Error: e,
            }));
            resp.status(500).end();
          }
        });

        server = appl.listen(options.port);
        logger.verbose('Inspection service listening at port ' + options.port.toString());
      },
      async () => {
        server?.close();
      },
    ),
  ];
}

export class InspectionService extends app.BaseActor {}
