import * as applib from '@darlean/app-lib';

export function obtainFactories(logger: applib.ILogger): applib.ActorFactory[] {
  return [
    applib.actorFactory(logger, {
      capacity: 10,
      clazz: LoggingActor,
    }),
  ];
}

export interface IPushStatsRequest {
  events: { [message: string]: { count: number } };
}

export interface ILogFilter {
  messages: { msg: string; action: 'show' | 'hide' }[];
}

export interface ILogEvent {
  message: string;
  level: string;
  args?: { [key: string]: unknown };
  moment: string;
  tags?: { [key: string]: unknown };
}

export interface IPushLogEventsRequest {
  events: ILogEvent[];
}

export interface IGetStatsResponse {
  stats: { message: string; count: number }[];
}

export interface IGetLogEventsResponse {
  events: ILogEvent[];
}

@applib.actor({})
export class LoggingActor extends applib.BaseActor {
  private stats: Map<string, { count: number }>;
  private messages: ILogEvent[];
  private filter: ILogFilter;
  private history: number;

  constructor(options: applib.IActorCreateOptions) {
    super(options);
    this.stats = new Map();
    this.messages = [];
    this.filter = {
      messages: [],
    };
    this.history = 10000;
  }

  @applib.action()
  public async pushStats(stats: IPushStatsRequest): Promise<ILogFilter> {
    for (const [msg, value] of Object.entries(stats.events)) {
      let current = this.stats.get(msg);
      if (!current) {
        current = { count: 0 };
        this.stats.set(msg, current);
      }
      current.count += value.count;
    }
    return this.filter;
  }

  @applib.action()
  public async pushLogEvents(events: IPushLogEventsRequest): Promise<void> {
    const nrdelete = this.messages.length + events.events.length - this.history;
    if (nrdelete > 0) {
      this.messages.splice(0, nrdelete);
    }
    for (const event of events.events) {
      this.messages.push(event);
    }
  }

  @applib.action()
  public async getStats(): Promise<IGetStatsResponse> {
    const result: IGetStatsResponse = {
      stats: [],
    };
    for (const [msg, info] of this.stats.entries()) {
      result.stats.push({
        message: msg,
        count: info.count,
      });
    }
    return result;
  }

  @applib.action()
  public async getLogEvents(): Promise<IGetLogEventsResponse> {
    this.c.info('Get log events');
    const result: IGetLogEventsResponse = {
      events: [],
    };
    for (const event of this.messages) {
      result.events.push(event);
    }
    return result;
  }

  @applib.action()
  public async getFilter(): Promise<ILogFilter> {
    return this.filter;
  }

  @applib.action()
  public async setFilter(filter: ILogFilter): Promise<void> {
    this.filter = filter;
  }
}
