import * as applib from '@darlean/app-lib';

export function obtainFactories(logger: applib.ILogger): applib.ActorFactory[] {
  return [
    applib.actorFactory(logger, {
      capacity: 10,
      clazz: TracingActor,
    }),
  ];
}

interface ICorrelationInfo {
  correlationId: string;
  segments: Map<string, applib.interface_tracing.ISegment>;
}

@applib.actor({ name: 'TracingActor' })
export class TracingActor extends applib.BaseActor {
  protected correlations: Map<string, ICorrelationInfo>;

  constructor(options: applib.IActorCreateOptions) {
    super(options);
    this.correlations = new Map();
  }

  @applib.action({ name: 'pushTraces' })
  public async pushTraces(traces: applib.interface_tracing.IPushSegmentsRequest): Promise<void> {
    // console.log('Received traces', JSON.stringify(traces));
    for (const segment of traces.segments) {
      for (const cid of segment.correlationIds) {
        let info = this.correlations.get(cid);
        if (!info) {
          info = {
            correlationId: cid,
            segments: new Map(),
          };
          this.correlations.set(cid, info);
        }
        info.segments.set(segment.id, segment);
      }
    }
  }

  @applib.action({ name: 'getTraces' })
  public async getTraces(): Promise<applib.interface_tracing.IGetTracesResponse> {
    return {
      correlationIds: Array.from(this.correlations.keys()),
    };
  }

  @applib.action({ name: 'getTrace' })
  public async getTrace(
    data: applib.interface_tracing.IGetTraceRequest,
  ): Promise<applib.interface_tracing.IGetTraceResponse> {
    // console.log('GETTING TRACE', data.correlationId);
    const info = this.correlations.get(data.correlationId);
    return {
      segments: info ? Array.from(info.segments.values()) : [],
    };
  }
}
