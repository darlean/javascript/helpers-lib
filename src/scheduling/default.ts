import * as app from '@darlean/app-lib';
import { action, actor, service } from '@darlean/app-lib';

export function obtainFactories(logger: app.ILogger): app.ActorFactory[] {
  let timeout: app.ITimer;
  return [
    app.actorFactory(logger, {
      clazz: SchedulingPeriodActor,
      capacity: 1000,
    }),
    app.actorFactory(logger, {
      clazz: SchedulingCoordinatorActor,
      capacity: 1000,
      onAppStart: async (runtime) => {
        timeout = start(runtime, logger);
      },
      onAppStop: async (_runtime) => {
        timeout.cancel();
      },
    }),
    app.actorFactory(logger, {
      clazz: SchedulingService,
      capacity: 1,
    }),
  ];
}

function start(runtime: app.IRuntime, logger: app.ILogger): app.ITimer {
  return runtime.time.repeat(
    async () => {
      await runtime.dispatchAction(logger, {
        actorType: 'SchedulingCoordinatorActor',
        actorId: [],
        actionName: 'step',
      });
    },
    'SchedulingTimer',
    60 * 1000,
    0,
  );
}

@actor({ name: 'ScedulingPeriodActor' })
export class SchedulingPeriodActor extends app.BaseActor {
  @action({ name: 'schedule' })
  public async schedule(
    request: app.interface_scheduling.IScheduleReminderRequest,
    context: app.IActionContext,
  ): Promise<void> {
    const state = context.getService('state') as app.IPersistenceService;

    await state.store({
      compartment: 'scheduling',
      lookupKeys: ['timers'],
      sortKeys: request.id,
      value: request,
    });
    this.c.deep('Scheduled action for [Id]', () => ({ Id: request.id }));
  }

  @action({ name: 'invoke' })
  public async invoke(data: void, context: app.IActionContext): Promise<void> {
    this.c.debug('Performing one scheduling invoke for period');

    const state = context.getService('state') as app.IPersistenceService;
    const sched = context.getService('scheduling') as app.ISchedulingService;
    const time = context.getService('time') as app.ITimeService;
    const now = await time.nodeTime();

    while (true) {
      const items = await state.query<app.interface_scheduling.IScheduleReminderRequest>({
        compartment: 'scheduling',
        lookupKeys: ['timers'],
        limit: 100,
      });

      if (items.items.length === 0) {
        break;
      }

      // TODO: Do this in parallel?
      // Fire and forget (do not wait) is already implemented.
      // How to handle flooding the system?
      for (const item of items.items) {
        const timer = item.value.struct();
        //if (timer.moment! <= now) {
        await this.processItemAndReschedule(timer, context, state, sched, now);
        //} else {
        // Reschedule the timer
        //    await sched.schedule(timer);
        // }
      }
    }
  }

  protected async processItemAndReschedule(
    timer: app.interface_scheduling.IScheduleReminderRequest,
    context: app.IActionContext,
    state: app.IPersistenceService,
    sched: app.ISchedulingService,
    now: number,
  ): Promise<void> {
    await state.delete({
      compartment: 'scheduling',
      lookupKeys: ['timers'],
      sortKeys: timer.id,
    });
    try {
      await context.performAction({
        actorType: timer.callback.actorType,
        actorId: timer.callback.actorId,
        actionName: timer.callback.actionName,
        // returnedData: timer.callback.returnData,
      });
    } finally {
      if (timer.interval !== undefined) {
        if (timer.repeatCount === undefined || timer.repeatCount > 0) {
          await sched.scheduleReminder({
            id: timer.id,
            interval: timer.interval,
            repeatCount: timer.repeatCount === undefined ? undefined : timer.repeatCount - 1,
            callback: timer.callback,
            moment: now + timer.interval,
          });
        }
      }
    }
  }
}

// TODO: Also register timer (and service?) by id, so that they can be deleted.

@actor({ name: 'SchedulingCoordinatorActor' })
export class SchedulingCoordinatorActor extends app.BaseActor {
  protected lastPeriod?: string[];

  @action({ name: 'schedule' })
  public async schedule(
    request: app.interface_scheduling.IScheduleReminderRequest,
    context: app.IActionContext,
  ): Promise<void> {
    const state = context.getService('state') as app.IPersistenceService;

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const period = this.derivePeriod(request.moment!);

    await state.store({
      compartment: 'scheduling',
      lookupKeys: ['periods'],
      sortKeys: period,
      value: { period },
    });

    await context.performAction({
      actorType: 'SchedulingPeriodActor',
      actorId: period,
      actionName: 'schedule',
      data: request,
    });
  }

  @action({ name: 'step' })
  public async step(data: undefined, context: app.IActionContext): Promise<void> {
    const state = context.getService('state') as app.IPersistenceService;
    const time = context.getService('time') as app.ITimeService;
    const now = await time.nodeTime();
    const nowPeriod = this.derivePeriod(now);
    if (this.lastPeriod !== undefined && app.encodeKey(this.lastPeriod) === app.encodeKey(nowPeriod)) {
      this.c.debug('Skipping scheduling step (already done for [Period])', () => ({ Period: nowPeriod }));
      return;
    }
    this.c.debug('Performing scheduling step for [Period]', () => ({ Period: nowPeriod }));
    this.lastPeriod = nowPeriod;
    while (true) {
      const periods = await state.query({
        compartment: 'scheduling',
        lookupKeys: ['periods'],
        end: {
          sortKeys: this.derivePeriod(now),
          compare: 'inclusive',
        },
        limit: 10,
        order: 'ascending',
      });

      this.c.deep('Found [n] periods', () => ({ n: periods.items.length }));

      if (periods.items.length === 0) {
        break;
      }

      for (const period of periods.items) {
        // TODO: Make deletion smarter (ensure that retry is performed when fire does not work)
        await state.delete({
          compartment: 'scheduling',
          lookupKeys: ['periods'],
          sortKeys: period.sortKeys,
        });

        await context.performAction({
          actorType: 'SchedulingPeriodActor',
          actorId: this.id.concat(period.sortKeys),
          actionName: 'invoke',
        });
      }
    }
  }

  protected derivePeriod(moment: number): string[] {
    const date = new Date(moment);
    return [
      date.getUTCFullYear().toString().padStart(4, '0'),
      (date.getUTCMonth() + 1).toString().padStart(2, '0'),
      date.getUTCDate().toString().padStart(2, '0'),
      date.getUTCHours().toString().padStart(2, '0'),
      date.getUTCMinutes().toString().padStart(2, '0'),
    ];
  }
}

@service({ name: 'SchedulingService' })
export class SchedulingService extends app.BaseActor {
  @action({ name: 'schedule' })
  public async schedule(
    request: app.interface_scheduling.IScheduleReminderRequest,
    context: app.IActionContext,
  ): Promise<void> {
    if (request.interval && request.interval < 0) {
      throw new Error(`Timer interval ${request.interval} must be >= 0`);
    }

    if (request.interval == 0 && request.repeatCount === undefined) {
      throw new Error(`Timer repeat count must be set when interval = 0`);
    }

    const time = context.getService('time') as app.ITimeService;
    const now = await time.nodeTime();

    const moment = (request.moment ?? now) + (request.delay ?? request.interval ?? 0);
    const req2: app.interface_scheduling.IScheduleReminderRequest = {
      id: request.id,
      callback: request.callback,
      interval: request.interval,
      repeatCount: request.repeatCount,
      moment,
    };

    await context.performAction({
      actorType: 'SchedulingCoordinatorActor',
      actorId: [],
      actionName: 'schedule',
      data: req2,
    });
  }
}
