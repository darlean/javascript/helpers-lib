import * as crc from 'crc';
import * as sqlite from 'better-sqlite3';
import * as app from '@darlean/app-lib';
import { action, IActionContext, ITimer, Mutex, parallel, Struct } from '@darlean/app-lib';
import * as fs from 'fs';
import { IInspectResponse, ILoadResponse } from '@darlean/app-lib/lib/interfaces/persistence';
//import * as sqlite3 from 'sqlite3-helper';
import * as sqlite3 from './sqlite-async';

export function obtainFactories(
  logger: app.ILogger,
  options: (compartment: string) => IFileSystemPersistenceOptions,
): app.ActorFactory[] {
  return [
    app.actorFactory(logger, {
      capacity: 1000,
      clazz: FileSystemPersistenceNodeActor,
      actorOptions: (id) => options(id[1]),
    }),
    app.actorFactory(logger, {
      capacity: 1000,
      clazz: FileSystemPersistenceNodeAsyncActor,
      actorOptions: (id) => options(id[1]),
    }),
    app.actorFactory(logger, {
      capacity: 100,
      clazz: FileSystemPersistenceService,
      actorOptions: (id) => options(id[0]),
    }),
  ];
}

const ACTOR = 'FileSystemPersistenceNodeAsyncActor';

/*function determineRangeUpperBound(prefix: string): string | undefined {
  // TODO: Handle case where codePointAt returns highest possible value
  if (prefix.length > 0) {
    return prefix.substr(0, prefix.length - 1) + String.fromCodePoint((prefix.codePointAt(prefix.length - 1) || 0) + 1);
  }
}*/

interface IQueueItem {
  request: app.Struct<app.interface_persistence.IStoreRequest>;
  resolve: () => void;
  reject: (reason: unknown) => void;
}

// Parts of the Id:
// - AppId
// - Compartment
// - Slicenumber

@app.actor({ locking: 'shared', appBindIndex: 0 })
export class FileSystemPersistenceNodeActor extends app.BaseActor {
  protected db?: sqlite.Database;
  protected sqlInsert?: sqlite.Statement;
  protected sqlDelete?: sqlite.Statement;
  protected sqlGetExact?: sqlite.Statement;
  protected sqlGetAll?: sqlite.Statement;
  protected sqlGetPrefixIIAsc?: sqlite.Statement;
  protected sqlGetPrefixIEAsc?: sqlite.Statement;
  protected sqlGetPrefixEIAsc?: sqlite.Statement;
  protected sqlGetPrefixEEAsc?: sqlite.Statement;
  protected sqlGetPrefixIIDesc?: sqlite.Statement;
  protected sqlGetPrefixIEDesc?: sqlite.Statement;
  protected sqlGetPrefixEIDesc?: sqlite.Statement;
  protected sqlGetPrefixEEDesc?: sqlite.Statement;

  protected queue: IQueueItem[];
  protected storing: boolean;

  constructor(request: app.IActorCreateOptions) {
    super(request);
    this.queue = [];
    this.storing = false;
  }

  public async activate(): Promise<void> {
    const filepath = ['fs'].concat(this.id).join('/');
    if (!fs.existsSync(filepath)) {
      fs.mkdirSync(filepath, { recursive: true });
    }

    const filename = [filepath, 'store.sqlite'].join('/');
    this.db = new sqlite(filename, {});
    // Only exclusive mode makes it possible to use the faster WAL without having a shared lock
    // (which we do not have, as the nodes run on different machines).
    this.db.pragma('locking_mode=EXCLUSIVE;');
    // Enabl;e the (faster) WAL mode
    this.db.pragma('journal_mode=WAL');

    this.db.exec('CREATE TABLE IF NOT EXISTS data (lookupkey, sortkey, value, PRIMARY KEY (lookupkey, sortkey))');

    this.sqlInsert = this.db.prepare('INSERT OR REPLACE INTO data (lookupkey, sortkey, value) VALUES (?, ?, ?)');
    this.sqlDelete = this.db.prepare('DELETE FROM data WHERE lookupkey=? AND sortkey=?');
    this.sqlGetExact = this.db.prepare('SELECT value FROM data WHERE lookupkey=? AND sortkey=?');

    this.sqlGetAll = this.db.prepare(
      'SELECT lookupkey, sortkey FROM data ORDER BY lookupkey ASC, sortkey ASC LIMIT 100000',
    );

    this.sqlGetPrefixIIAsc = this.db.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>=?)) AND ((? IS NULL) OR (sortkey<=?)) ORDER BY sortkey ASC LIMIT ?',
    );
    this.sqlGetPrefixIEAsc = this.db.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>=?)) AND ((? IS NULL) OR (sortkey<?)) ORDER BY sortkey ASC LIMIT ?',
    );
    this.sqlGetPrefixEIAsc = this.db.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>?)) AND ((? IS NULL) OR (sortkey<=?)) ORDER BY sortkey ASC LIMIT ?',
    );
    this.sqlGetPrefixEEAsc = this.db.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>?)) AND ((? IS NULL) OR (sortkey<?)) ORDER BY sortkey ASC LIMIT ?',
    );

    this.sqlGetPrefixIIDesc = this.db.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>=?)) AND ((? IS NULL) OR (sortkey<=?)) ORDER BY sortkey DESC LIMIT ?',
    );
    this.sqlGetPrefixIEDesc = this.db.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>=?)) AND ((? IS NULL) OR (sortkey<?)) ORDER BY sortkey DESC LIMIT ?',
    );
    this.sqlGetPrefixEIDesc = this.db.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>?)) AND ((? IS NULL) OR (sortkey<=?)) ORDER BY sortkey DESC LIMIT ?',
    );
    this.sqlGetPrefixEEDesc = this.db.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>?)) AND ((? IS NULL) OR (sortkey<?)) ORDER BY sortkey DESC LIMIT ?',
    );
  }

  @app.action({ name: 'store' })
  public async store(request: Struct<app.interface_persistence.IStoreRequest>): Promise<void> {
    //this.logger.warning('STORING BY ACTOR');
    return new Promise((resolve, reject) => {
      this.queue.push({
        request,
        resolve,
        reject,
      });
      this.triggerStore();
    });
  }

  @app.action({ name: 'delete' })
  public async delete(request: app.interface_persistence.IDeleteRequest): Promise<void> {
    const lookupkey = this.toKey(request.lookupKeys ?? []);
    const sortkey = this.toKey(request.sortKeys ?? []);
    // TODO: Put in queue??
    this.sqlDelete?.run(lookupkey, sortkey);
  }

  @app.action({ name: 'load' })
  public async load(request: app.interface_persistence.ILoadRequest): Promise<app.interface_persistence.ILoadResponse> {
    const lookupkey = this.toKey(request.lookupKeys ?? []);
    const sortkey = this.toKey(request.sortKeys ?? []);
    const results = this.sqlGetExact?.get(lookupkey, sortkey);
    if (results) {
      try {
        const buffer = results.value as Buffer;
        const att = app.decodeFromBinaryBuffer(buffer, 0);
        return app.struct<app.interface_persistence.ILoadResponse>((attach) => ({
          value: attach(att),
        }));
      } finally {
        results.finalize();
      }
    }
    return {};
  }

  @app.action({ name: 'query' })
  public async query(
    request: app.interface_persistence.IQueryRequest,
  ): Promise<app.interface_persistence.IQueryResponse> {
    this.c.deep('Performing query for lookup key [LookupKey]', () => ({
      LookupKey: request.lookupKeys,
    }));
    const lookupkey = this.toKey(request.lookupKeys ?? []);

    const startString = request.start ? this.toKey(request.start?.sortKeys ?? []) : undefined;

    const endString = request.end ? this.toKey(request.end?.sortKeys ?? []) : undefined;

    let statement: sqlite.Statement | undefined;
    if (request.order !== 'descending') {
      if (request.start?.compare === 'inclusive') {
        if (request.end?.compare === 'inclusive') {
          statement = this.sqlGetPrefixIIAsc;
        } else {
          statement = this.sqlGetPrefixIEAsc;
        }
      } else {
        if (request.end?.compare === 'inclusive') {
          statement = this.sqlGetPrefixEIAsc;
        } else {
          statement = this.sqlGetPrefixEEAsc;
        }
      }
    } else {
      if (request.start?.compare === 'inclusive') {
        if (request.end?.compare === 'inclusive') {
          statement = this.sqlGetPrefixIIDesc;
        } else {
          statement = this.sqlGetPrefixIEDesc;
        }
      } else {
        if (request.end?.compare === 'inclusive') {
          statement = this.sqlGetPrefixEIDesc;
        } else {
          statement = this.sqlGetPrefixEEDesc;
        }
      }
    }

    const limit = request.limit ?? 1000;

    if (statement) {
      const results: { lookupkey: string; sortkey: string; value: Buffer }[] = statement?.all(
        lookupkey,
        startString,
        startString,
        endString,
        endString,
        limit,
      );
      return app.struct((attach) => {
        const items = results?.map((v) => {
          const buffer = v.value as Buffer;
          const att = app.decodeFromBinaryBuffer(buffer, 0);
          return {
            lookupKeys: request.lookupKeys,
            sortKeys: this.fromKey(v.sortkey),
            value: attach(att),
          } as app.interface_persistence.IQueryResponseItem;
        });

        return {
          items,
        };
      });
    }
    return { items: [] };
  }

  public async deactivate(): Promise<void> {
    this.db?.close();
  }

  @app.action({ name: 'inspect' })
  public async inspect(_request: unknown): Promise<app.interface_persistence.IInspectResponse> {
    const results = this.sqlGetAll?.all();
    const result: app.interface_persistence.IInspectResponse = {
      items: [],
    };
    if (results) {
      for (const item of results) {
        const lookupkey = this.fromKey(item.lookupkey);
        const sortkey = this.fromKey(item.sortkey);
        result.items.push({
          lookupKeys: lookupkey,
          sortKeys: sortkey,
          shard: parseInt(this.id[2]),
        });
      }
    }
    return result;
  }

  protected toKey(values: string[]): string {
    // Character 1F is the "Unit Separator". Assume it is not present in values.
    return app.encodeKey(values);
  }

  protected fromKey(value: string): string[] {
    return app.decodeKey(value);
  }

  protected triggerStore(): void {
    if (this.storing) {
      return;
    }
    if (this.queue.length > 0) {
      this.storing = true;
      setTimeout(() => {
        try {
          this.storeQueue();
        } finally {
          this.storing = false;
          if (this.queue.length > 0) {
            this.triggerStore();
          }
        }
      }, 1);
    }
  }

  protected storeQueue(): void {
    const items = this.queue;
    this.queue = [];
    this.c.deep('Storing [Count] items', () => ({ Count: items.length }));
    try {
      this.db?.transaction(() => {
        for (const item of items) {
          const request = item.request;
          const lookupkey = this.toKey(request.lookupKeys ?? []);
          const sortkey = this.toKey(request.sortKeys ?? []);
          const att = item.request._att(item.request.value);
          const buf = app.encodeToBinaryBuffer(att);
          this.sqlInsert?.run(lookupkey, sortkey, buf);
          this.c.debug('Set value for lookup keys [LookupKeys] and sort keys [SortKeys]', () => ({
            LookupKeys: request.lookupKeys,
            SortKeys: request.sortKeys,
          }));
        }
      })();
      for (const item of items) {
        item.resolve();
      }
    } catch (e) {
      this.c.deep('Storing [Count] items resulted in error: [Error]', () => ({ Count: items.length, Error: e }));
      for (const item of items) {
        item.reject(e);
      }
    }
  }
}

@app.actor({ locking: 'shared', appBindIndex: 0 })
export class FileSystemPersistenceNodeAsyncActor extends app.BaseActor {
  protected readdb?: sqlite3.Database;
  protected writedb?: sqlite3.Database;
  protected sqlInsert?: sqlite3.StatementPool;
  protected sqlDelete?: sqlite3.StatementPool;
  protected sqlGetExact?: sqlite3.StatementPool;
  protected sqlGetAll?: sqlite3.StatementPool;
  protected sqlGetPrefixIIAsc?: sqlite3.StatementPool;
  protected sqlGetPrefixIEAsc?: sqlite3.StatementPool;
  protected sqlGetPrefixEIAsc?: sqlite3.StatementPool;
  protected sqlGetPrefixEEAsc?: sqlite3.StatementPool;
  protected sqlGetPrefixIIDesc?: sqlite3.StatementPool;
  protected sqlGetPrefixIEDesc?: sqlite3.StatementPool;
  protected sqlGetPrefixEIDesc?: sqlite3.StatementPool;
  protected sqlGetPrefixEEDesc?: sqlite3.StatementPool;
  protected sqlBeginTransaction?: sqlite3.StatementPool;
  protected sqlCommitTransaction?: sqlite3.StatementPool;
  protected sqlRollbackTransaction?: sqlite3.StatementPool;
  protected uncommitted: number;
  protected writeLock: Mutex<void>;
  protected commitTimer?: ITimer;

  constructor(request: app.IActorCreateOptions) {
    super(request);
    console.log('CREATE ACTOR', request);
    this.uncommitted = 0;
    this.writeLock = new Mutex();
  }

  public async activate(context: IActionContext): Promise<void> {
    const filepath = ['fs'].concat(this.id).join('/');
    if (!fs.existsSync(filepath)) {
      fs.mkdirSync(filepath, { recursive: true });
    }

    const filename = [filepath, 'store.sqlite'].join('/');
    const dbw = new sqlite3.Database();
    await dbw.open(filename, sqlite3.OPEN_CREATE | sqlite3.OPEN_READWRITE);
    this.writedb = dbw;

    // Only exclusive mode makes it possible to use the faster WAL without having a shared lock
    // (which we do not have, as the nodes run on different machines).
    await dbw.run('PRAGMA locking_mode=EXCLUSIVE;');
    // Enabl;e the (faster) WAL mode
    await dbw.run('PRAGMA journal_mode=WAL;');
    await dbw.run('CREATE TABLE IF NOT EXISTS data (lookupkey, sortkey, value, PRIMARY KEY (lookupkey, sortkey))');

    /*console.log('B');
    const dbr = new sqlite3.Database();
    await dbr.open(filename, sqlite3.OPEN_READWRITE);
    this.readdb = dbr;
    */
    const dbr = dbw;
    this.readdb = dbw;

    this.sqlBeginTransaction = await dbw.prepare('BEGIN IMMEDIATE TRANSACTION;');
    this.sqlCommitTransaction = await dbw.prepare('COMMIT TRANSACTION;');
    this.sqlRollbackTransaction = await dbw.prepare('ROLLBACK TRANSACTION;');

    this.sqlInsert = await dbw.prepare('INSERT OR REPLACE INTO data (lookupkey, sortkey, value) VALUES (?, ?, ?)');
    this.sqlDelete = await dbw.prepare('DELETE FROM data WHERE lookupkey=? AND sortkey=?');
    this.sqlGetExact = await dbr.prepare('SELECT value FROM data WHERE lookupkey=? AND sortkey=?');

    this.sqlGetAll = await dbr.prepare(
      'SELECT lookupkey, sortkey FROM data ORDER BY lookupkey ASC, sortkey ASC LIMIT 100000',
    );

    this.sqlGetPrefixIIAsc = await dbr.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>=?)) AND ((? IS NULL) OR (sortkey<=?)) ORDER BY sortkey ASC LIMIT ?',
    );
    this.sqlGetPrefixIEAsc = await dbr.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>=?)) AND ((? IS NULL) OR (sortkey<?)) ORDER BY sortkey ASC LIMIT ?',
    );
    this.sqlGetPrefixEIAsc = await dbr.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>?)) AND ((? IS NULL) OR (sortkey<=?)) ORDER BY sortkey ASC LIMIT ?',
    );
    this.sqlGetPrefixEEAsc = await dbr.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>?)) AND ((? IS NULL) OR (sortkey<?)) ORDER BY sortkey ASC LIMIT ?',
    );

    this.sqlGetPrefixIIDesc = await dbr.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>=?)) AND ((? IS NULL) OR (sortkey<=?)) ORDER BY sortkey DESC LIMIT ?',
    );
    this.sqlGetPrefixIEDesc = await dbr.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>=?)) AND ((? IS NULL) OR (sortkey<?)) ORDER BY sortkey DESC LIMIT ?',
    );
    this.sqlGetPrefixEIDesc = await dbr.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>?)) AND ((? IS NULL) OR (sortkey<=?)) ORDER BY sortkey DESC LIMIT ?',
    );
    this.sqlGetPrefixEEDesc = await dbr.prepare(
      'SELECT lookupkey, sortkey, value FROM data WHERE lookupkey=? AND ((? IS NULL) or (sortkey>?)) AND ((? IS NULL) OR (sortkey<?)) ORDER BY sortkey DESC LIMIT ?',
    );

    const tb = await this.sqlBeginTransaction?.obtain();
    try {
      await tb?.run();
    } finally {
      await tb?.release();
    }

    this.commitTimer = context.scheduleTimer({
      actionName: 'commit',
      interval: 50,
      name: 'AutoCommit',
    });
  }

  @app.action({ name: 'store', locking: 'none' })
  public async store(request: Struct<app.interface_persistence.IStoreRequest>): Promise<void> {
    await this.writeLock.acquire();
    try {
      //await setImmediate();
      //this.logger.warning('STORING BY ACTOR');
      const lookupkey = this.toKey(request.lookupKeys ?? []);
      const sortkey = this.toKey(request.sortKeys ?? []);
      const att = request._att(request.value);
      const buf = app.encodeToBinaryBuffer(att);
      const s = await this.sqlInsert?.obtain();
      try {
        await s?.run([lookupkey, sortkey, buf]);
        this.c.debug('Set value for lookup keys [LookupKeys] and sort keys [SortKeys]', () => ({
          LookupKeys: request.lookupKeys,
          SortKeys: request.sortKeys,
        }));
        this.uncommitted++;
      } finally {
        await s?.release();
      }
    } finally {
      this.writeLock.release();
    }
  }

  @app.action({ name: 'commit', locking: 'none' })
  public async commit(): Promise<void> {
    await this.writeLock.acquire();
    try {
      if (this.uncommitted > 0) {
        const tc = await this.sqlCommitTransaction?.obtain();
        try {
          await tc?.run();
        } finally {
          await tc?.release();
        }

        const tb = await this.sqlBeginTransaction?.obtain();
        try {
          await tb?.run();
        } finally {
          await tb?.release();
        }

        this.uncommitted = 0;
      }
    } finally {
      this.writeLock.release();
    }
  }

  @app.action({ name: 'delete' })
  public async delete(request: app.interface_persistence.IDeleteRequest): Promise<void> {
    await this.writeLock.acquire();
    try {
      const lookupkey = this.toKey(request.lookupKeys ?? []);
      const sortkey = this.toKey(request.sortKeys ?? []);
      const s = await this.sqlDelete?.obtain();
      try {
        await s?.run([lookupkey, sortkey]);
      } finally {
        await s?.release();
      }
      this.uncommitted++;
    } finally {
      this.writeLock.release();
    }
  }

  @app.action({ name: 'load', locking: 'exclusive' })
  public async load(request: app.interface_persistence.ILoadRequest): Promise<app.interface_persistence.ILoadResponse> {
    const lookupkey = this.toKey(request.lookupKeys ?? []);
    const sortkey = this.toKey(request.sortKeys ?? []);
    const s = await this.sqlGetExact?.obtain();
    try {
      const results = await s?.get<{ value: Buffer }>([lookupkey, sortkey]);
      if (results) {
        const buffer = results.value;
        const att = app.decodeFromBinaryBuffer(buffer, 0);
        return app.struct<app.interface_persistence.ILoadResponse>((attach) => ({
          value: attach(att),
        }));
      }
      return {};
    } finally {
      await s?.release();
    }
  }

  @app.action({ name: 'query' })
  public async query(
    request: app.interface_persistence.IQueryRequest,
  ): Promise<app.interface_persistence.IQueryResponse> {
    this.c.deep('Performing query for lookup key [LookupKey]', () => ({
      LookupKey: request.lookupKeys,
    }));
    const lookupkey = this.toKey(request.lookupKeys ?? []);

    const startString = request.start ? this.toKey(request.start?.sortKeys ?? []) : undefined;

    const endString = request.end ? this.toKey(request.end?.sortKeys ?? []) : undefined;

    let statement: sqlite3.StatementPool | undefined;
    if (request.order !== 'descending') {
      if (request.start?.compare === 'inclusive') {
        if (request.end?.compare === 'inclusive') {
          statement = this.sqlGetPrefixIIAsc;
        } else {
          statement = this.sqlGetPrefixIEAsc;
        }
      } else {
        if (request.end?.compare === 'inclusive') {
          statement = this.sqlGetPrefixEIAsc;
        } else {
          statement = this.sqlGetPrefixEEAsc;
        }
      }
    } else {
      if (request.start?.compare === 'inclusive') {
        if (request.end?.compare === 'inclusive') {
          statement = this.sqlGetPrefixIIDesc;
        } else {
          statement = this.sqlGetPrefixIEDesc;
        }
      } else {
        if (request.end?.compare === 'inclusive') {
          statement = this.sqlGetPrefixEIDesc;
        } else {
          statement = this.sqlGetPrefixEEDesc;
        }
      }
    }

    const limit = request.limit ?? 1000;

    if (statement) {
      const s = await statement.obtain();
      try {
        const results = await s?.all<{ lookupkey: string; sortkey: string; value: Buffer }>([
          lookupkey,
          startString,
          startString,
          endString,
          endString,
          limit,
        ]);
        return app.struct((attach) => {
          const items = results?.map((v) => {
            const buffer = v.value as Buffer;
            const att = app.decodeFromBinaryBuffer(buffer, 0);
            return {
              lookupKeys: request.lookupKeys,
              sortKeys: this.fromKey(v.sortkey),
              value: attach(att),
            } as app.interface_persistence.IQueryResponseItem;
          });

          return {
            items,
          };
        });
      } finally {
        await s.release();
      }
    }
    return { items: [] };
  }

  public async deactivate(): Promise<void> {
    const tc = await this.sqlCommitTransaction?.obtain();
    try {
      await tc?.run();
    } finally {
      await tc?.release();
    }

    this.readdb?.close();
    this.writedb?.close();
  }

  @app.action({ name: 'inspect' })
  public async inspect(_request: unknown): Promise<app.interface_persistence.IInspectResponse> {
    const s = await this.sqlGetAll?.obtain();
    try {
      const results = await s?.all<{ lookupkey: string; sortkey: string; value: Buffer }>();
      const result: app.interface_persistence.IInspectResponse = {
        items: [],
      };
      if (results) {
        for (const item of results) {
          const lookupkey = this.fromKey(item.lookupkey);
          const sortkey = this.fromKey(item.sortkey);
          result.items.push({
            lookupKeys: lookupkey,
            sortKeys: sortkey,
            shard: parseInt(this.id[2]),
          });
        }
      }
      return result;
    } finally {
      await s?.release();
    }
  }

  protected toKey(values: string[]): string {
    // Character 1F is the "Unit Separator". Assume it is not present in values.
    return app.encodeKey(values);
  }

  protected fromKey(value: string): string[] {
    return app.decodeKey(value);
  }
}

export interface IFileSystemPersistenceOptions {
  shardCount?: number;
  nodes: string[];
  redundancy: number;
  distribution: 'adjacent' | 'interleaved';
}

// TODO: Implement the RAFT mechanism with separate "leader" actor that synchronizes
// with the individual nodes.

@app.service({
  name: 'FileSystemPersistenceService',
  locking: 'shared',
  dependencies: ['FileSystemPersistenceNodeActor'],
})
export class FileSystemPersistenceService extends app.BaseActor {
  protected shardCount: number;
  protected redundancy: number;
  protected nodes: string[];
  protected distribution: 'adjacent' | 'interleaved';
  protected placementDelta: number;

  constructor(request: app.IActorCreateOptions & IFileSystemPersistenceOptions) {
    super(request);
    // console.log('CREATE SERVICE', request);
    this.shardCount = request.shardCount || 1024;
    this.nodes = request.nodes;
    this.redundancy = request.redundancy;
    this.distribution = request.distribution;
    this.placementDelta = this.distribution === 'adjacent' ? 1 : Math.floor(this.nodes.length / this.redundancy);
  }

  @action({ name: 'store', locking: 'none' })
  public async store(request: app.interface_persistence.IStoreRequest, context: app.IActionContext): Promise<void> {
    //this.logger.warning('STORING BY SERVICE');
    const tasks: app.ParallelTask<unknown, unknown>[] = [];
    const placement = this.derivePlacement(request.lookupKeys);
    // console.log('PLACEMENT', placement);
    for (const node of placement.nodes) {
      tasks.push(async (abort) => {
        try {
          return await context.performAction({
            actorType: ACTOR,
            actorId: [node, request.compartment || 'default', placement.shard],
            actionName: 'store',
            data: request,
            receivers: [node],
          });
        } catch (e) {
          abort(e);
        }
      });
    }
    const result = await parallel(tasks, 2000);
    if (result.result) {
      throw result.result;
    }
  }

  @action({ name: 'delete' })
  public async delete(
    request: app.interface_persistence.IDeleteRequest,
    context: app.IActionContext,
  ): Promise<unknown> {
    const tasks: app.ParallelTask<unknown, unknown>[] = [];
    const placement = this.derivePlacement(request.lookupKeys);
    for (const node of placement.nodes) {
      tasks.push(async (abort) => {
        try {
          return await context.performAction({
            actorType: ACTOR,
            actorId: [node, request.compartment || 'default', placement.shard],
            actionName: 'delete',
            data: request,
          });
        } catch (e) {
          abort(e);
        }
      });
    }
    const result = await parallel(tasks, 2000);
    if (result.result) {
      throw result.result;
    }
    return undefined;
  }

  @action({ name: 'load' })
  public async load(
    request: app.interface_persistence.ILoadRequest,
    context: app.IActionContext,
  ): Promise<ILoadResponse | undefined> {
    const placement = this.derivePlacement(request.lookupKeys);
    const elected = placement.nodes[0];
    return await context.performAction<app.interface_persistence.ILoadRequest, app.interface_persistence.ILoadResponse>(
      {
        actorType: ACTOR,
        actorId: [elected, request.compartment || 'default', placement.shard],
        actionName: 'load',
        data: request,
      },
    );
  }

  @app.action({ name: 'query' })
  public async query(
    request: app.interface_persistence.IQueryRequest,
    context: app.IActionContext,
  ): Promise<app.interface_persistence.IQueryResponse> {
    const placement = this.derivePlacement(request.lookupKeys);
    const elected = placement.nodes[0];
    return await context.performAction({
      actorType: ACTOR,
      actorId: [elected, request.compartment || 'default', placement.shard],
      actionName: 'query',
      data: request,
    });
  }

  @action({ name: 'inspect' })
  public async inspect(request: app.IInspectRequest, context: app.IActionContext): Promise<IInspectResponse> {
    const tasks: app.ParallelTask<IInspectResponse | undefined, void>[] = [];
    for (let shard = 0; shard < this.shardCount; shard++) {
      const placement = this.derivePlacement(shard);
      tasks.push(async () => {
        return await context.performAction<undefined, IInspectResponse>({
          actorType: ACTOR,
          actorId: [placement.nodes[0], request.compartment || 'default', placement.shard],
          actionName: 'inspect',
          data: undefined,
        });
      });
    }
    const results = await app.parallel(tasks, 2500, 25);
    const items: app.interface_persistence.IInspectResponseItem[] = [];
    for (let shard = 0; shard < this.shardCount; shard++) {
      const result = results.results[shard];
      if (result.error) {
        this.c.error('Error while inspecting shard [Shard]: [Error]', () => ({
          Shard: shard,
          Error: result.error,
        }));
      } else {
        for (const res of (result?.result as IInspectResponse)?.items ?? []) {
          items.push(res);
        }
      }
    }
    return {
      items,
    };
  }

  protected derivePlacement(partitionKey: string[] | number): { shard: string; nodes: string[] } {
    let offset = 0;
    if (typeof partitionKey === 'number') {
      offset = partitionKey;
    } else {
      let hash = 0;
      for (const part of partitionKey as string[]) {
        hash = crc.crc16(part, hash);
      }
      offset = hash % this.shardCount;
    }
    const result: { shard: string; nodes: string[] } = {
      shard: offset.toString().padStart(6, '0'),
      nodes: [],
    };
    const delta = this.placementDelta;
    for (let shardnrraw = offset; shardnrraw < offset + this.redundancy; shardnrraw += delta) {
      const shardnr = shardnrraw % this.shardCount;
      const node = this.nodes[shardnr % this.nodes.length];
      result.nodes.push(node);
    }
    return result;
  }
}
