import * as app from '@darlean/app-lib';
import { IInspectRequest } from '@darlean/app-lib';
import { IInspectResponse } from '@darlean/app-lib/lib/interfaces/persistence';

export function obtainFactories(logger: app.ILogger, options: IPersistenceOptions): app.ActorFactory[] {
  return [
    app.actorFactory(logger, {
      capacity: 100,
      clazz: PersistenceService,
      actorOptions: () => options,
    }),
  ];
}

export interface IPersistenceCompartment {
  name: string;
  actorType: string;
}

export interface IPersistenceOptions {
  compartments: IPersistenceCompartment[];
}

@app.service({
  name: 'PersistenceService',
  locking: 'shared',
  dependencies: ['FileSystemPersistenceService'],
})
export class PersistenceService extends app.BaseActor {
  protected compartments: Map<string, IPersistenceCompartment>;

  constructor(request: app.IActorCreateOptions & IPersistenceOptions) {
    super(request);
    this.compartments = new Map();
    for (const compartment of request.compartments) {
      this.compartments.set(compartment.name, compartment);
    }
  }

  @app.action({ name: 'store', locking: 'none' })
  public async store(request: app.interface_persistence.IStoreRequest, context: app.IActionContext): Promise<void> {
    const compartment = this.findCompartment(request.compartment);
    await context.performAction<typeof request, void>({
      actorType: compartment.actorType,
      actorId: [request.compartment || 'default'],
      actionName: 'store',
      data: request,
    });
  }

  @app.action({ name: 'delete' })
  public async delete(request: app.interface_persistence.IDeleteRequest, context: app.IActionContext): Promise<void> {
    const compartment = this.findCompartment(request.compartment);
    await context.performAction<app.interface_persistence.IDeleteRequest, void>({
      actorType: compartment.actorType,
      actorId: [],
      actionName: 'delete',
      data: request,
    });
  }

  @app.action({ name: 'load' })
  public async load(
    request: app.interface_persistence.ILoadRequest,
    context: app.IActionContext,
  ): Promise<app.interface_persistence.ILoadResponse> {
    const compartment = this.findCompartment(request.compartment);
    return await context.performAction<app.interface_persistence.ILoadRequest, app.interface_persistence.ILoadResponse>(
      {
        actorType: compartment.actorType,
        actorId: [],
        actionName: 'load',
        data: request,
      },
    );
  }

  @app.action({ name: 'query' })
  public async query(
    request: app.interface_persistence.IQueryRequest,
    context: app.IActionContext,
  ): Promise<app.interface_persistence.IQueryResponse> {
    const compartment = this.findCompartment(request.compartment);
    return await context.performAction({
      actorType: compartment.actorType,
      actorId: [],
      actionName: 'query',
      data: request,
    });
  }

  @app.action({ name: 'inspect' })
  public async inspect(request: IInspectRequest, context: app.IActionContext): Promise<IInspectResponse> {
    const compartment = this.findCompartment(request.compartment);
    const result = await context.performAction<IInspectRequest, IInspectResponse>({
      actorType: compartment.actorType,
      actorId: [],
      actionName: 'inspect',
      data: request,
    });
    return result;
  }

  protected findCompartment(name?: string): IPersistenceCompartment {
    const result = this.compartments.get(name || 'default');
    if (!result) {
      throw new Error(`Persistence compartment [${name || 'default'}] is not registered`);
    }
    return result;
  }
}
