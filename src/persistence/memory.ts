export const MEMORY_TO_DO = true;

/*import * as app from '@darlean/app-lib';
import * as crc from 'crc';

export function obtainFactories(logger: app.ILogger): app.ActorFactory[] {
  return [
    new app.ActorFactory(
      logger,
      'MemoryPersistenceActor',
      'actor',
      1000,
      [],
      (options) => new MemoryPersistenceActor(options),
    ),
    new app.ActorFactory(
      logger,
      'MemoryPersistenceService',
      'service',
      1,
      [],
      (options) => new MemoryPersistenceService(options),
    ),
  ];
}

export class MemoryPersistenceActor extends app.BaseActor {
  protected items?: Map<string, ArrayBuffer>;

  public async activate(): Promise<void> {
    this.items = new Map();
  }

  public async store(request: app.persistence.IStoreRequest<ArrayBuffer>): Promise<void> {
    const key = this.toKey((request.lookupKeys ?? []).concat(request.sortKeys ?? []));
    this.items?.set(key, request.value);
    this.logger.deep('Set value for lookup keys [LookupKeys] and sort keys [SortKeys]', () => ({
      LookupKeys: request.lookupKeys,
      SortKeys: request.sortKeys,
    }));
  }

  public async delete(request: app.persistence.IDeleteRequest): Promise<void> {
    const key = this.toKey((request.lookupKeys ?? []).concat(request.sortKeys ?? []));
    this.items?.delete(key);
  }

  public async load(request: app.persistence.ILoadRequest): Promise<ArrayBuffer | undefined> {
    const key = this.toKey((request.lookupKeys ?? []).concat(request.sortKeys ?? []));
    return this.items?.get(key);
  }

  public async query(request: app.persistence.IQueryRequest): Promise<app.persistence.IQueryResponse<ArrayBuffer>> {
    this.logger.deep('Performing query for lookup key [LookupKey] in [N] items', () => ({
      LookupKey: request.lookupKeys,
      N: this.items?.size,
    }));

    const startString = request.start
      ? this.toKey((request.lookupKeys ?? []).concat(request.start?.sortKeys ?? []))
      : '';
    const endString = request.end ? this.toKey((request.lookupKeys ?? []).concat(request.end?.sortKeys ?? [])) : '';

    const results = [];
    // TODO: Optimize this linear search
    if (this.items) {
      for (const [key, value] of this.items?.entries()) {
        const startOk =
          startString === '' || (request.start?.compare === 'exclusive' ? key > startString : key >= startString);
        this.logger.deep('Start is [StartOk] for key [Key] and start string [StartString]', () => ({
          StartOk: startOk,
          Key: key,
          StartString: startString,
        }));
        if (startOk) {
          const endOk = endString === '' || (request.end?.compare === 'exclusive' ? key < endString : key <= endString);
          this.logger.deep('End is [EndOk] for key [Key] and end string [EndString]', () => ({
            EndOk: endOk,
            Key: key,
            EndString: endString,
          }));
          if (endOk) {
            results.push({
              lookupKeys: request.lookupKeys,
              sortKeys: this.fromKey(key).slice(request.lookupKeys.length),
              value,
            } as app.persistence.IQueryResponseItem<ArrayBuffer>);
          }
        }
      }
    }
    if (request.order) {
      results.sort((a, b) => (a > b ? 1 : a === b ? 0 : -1));
      if (request.order === 'descending') {
        results.reverse();
      }
    }
    return {
      items: results.slice(0, request.limit),
    };
  }

  public async deactivate(): Promise<void> {
    this.items = undefined;
  }

  protected toKey(values: string[]): string {
    // Character 1F is the "Unit Separator". Assume it is not present in values.
    return app.encodeKey(values);
  }

  protected fromKey(value: string): string[] {
    return app.decodeKey(value);
  }
}

export class MemoryPersistenceService extends app.BaseActor {
  public async store(request: app.persistence.IStoreRequest<ArrayBuffer>, context: app.IActionContext): Promise<void> {
    await context.performAction({
      actorType: 'MemoryPersistenceActor',
      actorId: this.deriveShardId(request.lookupKeys),
      actionName: 'store',
      data: request,
    });
  }

  public async delete(request: app.persistence.IDeleteRequest, context: app.IActionContext): Promise<unknown> {
    return await context.performAction({
      actorType: 'MemoryPersistenceActor',
      actorId: this.deriveShardId(request.lookupKeys),
      actionName: 'delete',
      data: request,
    });
  }

  public async load(
    request: app.persistence.ILoadRequest,
    context: app.IActionContext,
  ): Promise<ArrayBuffer | undefined> {
    return (await context.performAction({
      actorType: 'MemoryPersistenceActor',
      actorId: this.deriveShardId(request.lookupKeys),
      actionName: 'load',
      data: request,
    })) as ArrayBuffer | undefined;
  }

  public async query(
    request: app.persistence.IQueryRequest,
    context: app.IActionContext,
  ): Promise<app.persistence.IQueryResponse<ArrayBuffer>> {
    return (await context.performAction({
      actorType: 'MemoryPersistenceActor',
      actorId: this.deriveShardId(request.lookupKeys),
      actionName: 'query',
      data: request,
    })) as app.persistence.IQueryResponse<ArrayBuffer>;
  }

  protected deriveShardId(partitionKey: string[]): string[] {
    let hash = 0;
    for (const part of partitionKey) {
      hash = crc.crc16(part, hash);
    }
    return this.id.concat([hash.toString()]);
  }
}
*/
